<?php
header("Content-type: text/html; charset=utf-8");

include(dirname(__FILE__) . '\setup.php');
include(APP_PATH . '\const.php');
include(APP_PATH . '\config.php');
include(APP_PATH . '\function.php');

$_SESSION['token'] = hash("sha256", time());
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>音乐爸爸</title>
<meta http-equiv="x-dns-prefetch-control" content="on">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0">
<?php addJs('jquery-3.4.1.min.js', true);?>
<?php addJs('jquery-ui.min.js', true);?>
<?php addJs('jquery.blockUI.js', true);?>
<?php addJs('common.js');?>
<?php addJs('index.js');?>
<?php addCss('font.css');?>
<?php addCss('setup.css');?>
<?php addCss('base.css');?>
<?php addCss('jquery-ui.min.css');?>
<script type="text/javascript">
	var token = '<?php echo $_SESSION['token']?>';
</script>
</head>
<body>
	<header>
		<div class="header">
			<div class="header-menu-wrap">
				<div class="f1">
					<div class="main-menu-wrap">
						<div class="main-menu-box"></div>
						<div class="main-menu-sub">
							<ul>
								<li class="btn-reset-like">重置喜欢</li>
								<li class="btn-trash-like">恢复删除</li>
								<li class="bound-line"></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="f1 header_title">爸爸音乐</div>
				<div class="f1 tr">
					<div class="music-count-wrap">
						<span class="music-count">0</span>
						<span class="music-count-unit">首歌</span>
					</div>
				</div>
			</div>
		</div>
	</header>
	<content>
		<div class="content">
			<ul class="play-list"></ul>
			<ul class="drag-list" style="display: none;"></ul>
			<div class="player-wrap">
				<div class="player-body">
					<div class="music-category">分类</div>
					<div class="music-title">音乐文件标题</div>
					<div class="control-btn df pTLR40">
						<div class="f1 tc">
							<div class="btn-trash trash-btn mt0"></div>
						</div>
						<div class="f1 tc">
							<div class="btn-like like-btn"></div>
						</div>
					</div>
					<div class="player-box"></div>
					<div class="control-btn">
						<div class="btn-prev btn-box btn-gray">上一首</div>
						<div class="btn-next btn-box btn-gray ml20">下一首</div>
					</div>
					<div class="btn-wrap">
						<div class="btn-close btn-box btn-red">关闭</div>
					</div>
				</div>
				<div class="player-mini"></div>
			</div>
			<div class="audio-wrap hidden"></div>
		</div>
	</content>
	<footer>
		<div class="footer">
			<div class="footer-menu-wrap">
				<div class="df f-menu-0">
					<div class="f1">
						<div class="selected-count">0</div>
					</div>
					<div class="f1 tr">
						<div class="btn-box next-btn disable">下一步</div>
					</div>
				</div>
				<div class="df f-menu-1" style="display: none;">
					<div class="f1">
						<div class="btn-box prev-btn">返回</div>
					</div>
					<div class="f1 tr">
						<div class="btn-box finish-btn">完成</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>