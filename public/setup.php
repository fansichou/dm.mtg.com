<?php
// 公开路径
if (!defined('PUBLIC_PATH')) define('PUBLIC_PATH', $_SERVER['DOCUMENT_ROOT']);
// 开发跟路径
if (!defined('ROOT_PATH')) define('ROOT_PATH', substr(PUBLIC_PATH, 0, strrpos(PUBLIC_PATH, '/')));
// App路径
if (!defined('APP_PATH')) define('APP_PATH', ROOT_PATH . '/app');
// 音乐文件夹
if (!defined('MUSIC_FORDER')) define('MUSIC_FORDER', 'music');
// 音乐路径
if (!defined('MUSIC_PATH')) define('MUSIC_PATH', PUBLIC_PATH . '/' . MUSIC_FORDER);
// 保存路径
if (!defined('SAVE_PATH')) define('SAVE_PATH', PUBLIC_PATH . '/save');
?>