$(function(){
	var order_number = $('select.order_input');
	// 得到列表
	var json_data = JSON.parse(data);
	// 列表选择框
	
	$.each(json_data, function(i, obj){
		$('<option value="' + obj.name + '">').text(obj.name).appendTo(order_number);
	})
	
	
	var send = function(){
		// 向服务器发送结果
		$.ajax({
			url: 'copy.php',
			type: 'post',
			dataType: 'json',
			data: {
				token: token,
				order: order_number.val(),
			},
			beforeSend: function(){
				// 显示正在等待
				$.blockUI({
					message: '<div class="waiting"></div>',
					css: {
						width: '50%',
						padding: '10px 30px',
						border: 'none',
						borderRadius: '10px',
						left: '25%',
					}
				});
			},
			success: function(result){
				if (!result.error && result.status !== 1){
					result.error = 999;
					result.message = '未知错误, 非正常请求';
				}
				if (result.error){
					$('<div>').text(result.message).dialog({
						title: '错误:' + result.error,
						buttons: [
							{
								text: '确定',
								icon: 'ui-icon-alert',
								click: function() {
									$(this).dialog('destroy');
								}
							},
						]
					});
					return;
				}
				
				$('<div>').text(result.message).dialog({
					title: '操作完成',
					buttons: [
						{
							text: '确定',
							icon: 'ui-icon ui-icon-check',
							click: function() {
								$(this).dialog('destroy');
								order_number.focus();
							}
						},
					]
				});
			},
			error: function(result){
				// alert('未知错误');
			},
			complete: function(){
				// 隐藏正在等待
				$.unblockUI();
			}
		});
	}
	
	
	$('.make-btn').on('click', function(){
		if (order_number.val() == ''){
			order_number.focus();
			return;
		}
		send();
	});
	
	$('.prev-btn').on('click', function(){
		location.href='/';
	});
});