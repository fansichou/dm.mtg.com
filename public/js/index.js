$(function(){
	// 存储.分类
	var storage_category = new storageController('dm_category', true);
	// 存储.喜欢
	var storage_like = new storageController('dm_like', true);
	// 以使用过的喜欢
	var storage_used_like = new storageController('dm_used_like', true);
	// 以删除的音乐
	var storage_trash = new storageController('dm_trash', true);
	
	// 分类与音乐数据， 从服务器生成
	var json_data = {};
	// 下一步 按钮
	var next_btn = $('.next-btn');
	// 底部已选择
	var selected_count_elem = $('.selected-count');
	// 清空主容器
	var play_list = $('.play-list').html('');
	var drag_list = $('.drag-list').html('');
	var audio_wrap = $('.audio-wrap').html('');
	// 已选择数量
	var selected_count = 0;
	
	// 可选择最大时长 63分钟
	var max_select_time = 60*63;
	// 已选择文件的时长
	var selected_time = 0;
	
	// 正在工作的播放器
	var the_working_play_btn = null;
	// 播放器打开状态
	var player_box_display = false;
	// 播放器节点
	var player_box = $('.player-wrap');
	// 播放器元件区域
	var player_box_elem = player_box.find('.player-box');
	// 播放器按钮-删除
	var player_box_btn_trash = player_box.find('.btn-trash');
	// 播放器按钮-喜欢
	var player_box_btn_like = player_box.find('.btn-like');
	// 播放器按钮-上一首
	var player_box_btn_prev = player_box.find('.btn-prev');
	// 播放器按钮-下一首
	var player_box_btn_next = player_box.find('.btn-next');
	// 播放器按钮-关闭
	var player_box_btn_close = player_box.find('.btn-close');
	
	var getList = function(){
		
		$.ajax({
			url: 'list.php',
			type: 'post',
			dataType: 'json',
			data: {
				token: token,
			},
			beforeSend: function(){
				// 显示正在等待
				$.blockUI({
					message: '<div class="waiting"></div>',
					css: {
						width: '50%',
						padding: '10px 30px',
						border: 'none',
						borderRadius: '10px',
						left: '25%',
					}
				});
			},
			success: function(result){
				if (!result.error && result.status !== 1){
					result.error = 999;
					result.message = '未知错误, 非正常请求';
				}
				if (result.error){
					$('<div>').html(result.message).dialog({
						title: '错误:' + result.error,
						modal: true,
						buttons: [
							{
								text: '确定',
								icon: 'ui-icon-alert',
								click: function() {
									$(this).dialog('destroy');
								}
							},
						]
					});
					return;
				}
				
				json_data = result.list;
				// 没有音乐文件时, 显示提示, 并 10秒/次 刷新页面
				if ($.isEmptyObject(json_data)){
					play_list.text('暂时没有音乐可显示, 请稍后再试');
					setTimeout(function(){
						location.reload();
					}, 10000);
					return;
				}
				
				printMainPage();
				
			},
			error: function(result){
				//alert('未知错误');
			},
			complete: function(){
				// 隐藏正在等待
				$.unblockUI();
			}
		});
	}
	getList();
	
	// 设置总选择数量
	var setSelectedCount = function(method)
	{
		selected_count = 0;
		$.each(json_data, function(key, data) {
			$.each(data, function(i, obj){
				if (
					storage_like.get(obj.path, false)
					&& !storage_used_like.get(obj.path, false)
					&& !storage_trash.get(obj.path, false)
				) selected_count ++;
			});
		});
		selected_count_elem.text(selected_count);
		if (selected_count < 10){
			next_btn.addClass('disable');
		}else{
			next_btn.removeClass('disable');
		}
	}
	
	// 秒格式化为分:秒
	var formatTime = function(result){
		var fNum = function(num){
			return num < 10? ('0' + num): num.toString();
		}
		var h = fNum(Math.floor(result / 3600));
		var m = fNum(Math.floor((result / 60 % 60)));
		var s = fNum(Math.floor((result % 60)));
		
		return (h != '00'? (h + ':'): '') + m + ':' + s;
	}
	
	// 设置标题栏信息, 喜欢,音乐数量, 没有音乐时删除该分类.
	var setCategoryInfo = function(category){
		var category_body = category.find('.category-body');
		var play_boxs = category_body.children('.play-box');
		if (play_boxs.length) {
			var new_height = play_boxs.length * 120;
			category_body.height(new_height).data('height', new_height);
			category.find('.count').text(play_boxs.length);
			var like_count = 0;
			$.each(play_boxs, function(i, play_box){
				var data = $(play_box).data('data');
				if (data.like) like_count ++;
			});
			category.find('.like-count').text(like_count);
		} else {
			category.remove();
		}
	}
	
	// 设置按钮状态
	var setButtonStatus = function(json, status){
		status = status? true: false;
		var is_plaing = json.is_plaing;
		var play_btn = json.play_btn;
		var play_btn_text = play_btn.children('.play-btn-text');
		var obj = json.obj;
		if (status){
			// 播放暂停时动作
			// 标题栏隐藏 正在播放的图标
			is_plaing.removeClass('play');
			// 设置播放按钮的文字与风格
			play_btn.closest('.play-box').removeClass('playing');
			play_btn_text.text('以暂停');
			obj.paused = true;
		}else{
			// 播放开始时动作
			// 标题栏显示 正在播放图标
			is_plaing.addClass('play');
			// 设置播放按钮的文字与风格
			play_btn.closest('.play-box').addClass('playing');
			play_btn_text.text('正在播放');
			obj.paused = false;
		}
	}
	
	// 返回播放器控件
	var getPlayObj = function(json){
		var obj = json.obj;
		// [播放]按钮
		var play_btn = json.play_btn;
		var is_plaing = json.is_plaing;
		// [播放]按钮的文字
		var play_btn_text = play_btn.children('.play-btn-text');
		// [播放]按钮的时间now
		var time_now = play_btn.find('.time-now');
		// [播放]按钮的时间to
		var time_to = play_btn.find('.time-to');
		
		var player = $('<audio>')
		.text('您的浏览器无法播放音乐')
		.attr('controls', true)
		.attr('autoplay', true)
		.attr('loop', false)
		.attr('src', obj.url)
		.addClass('player')
		.on('loadstart', function(){
			play_btn_text.text('准备中');
		})
		.on('loadedmetadata', function(){
			time_to.text(formatTime(player.get(0).duration));
		})
		.on('play', function(){
			player.get(0).currentTime = play_btn.data('current_time') || 0;
			setButtonStatus({
				obj: obj,
				is_plaing: is_plaing,
				play_btn: play_btn,
			});
			the_working_play_btn = play_btn;
		})
		.on('pause', function(){
			if (player.get(0).currentTime == player.get(0).duration){
				play_btn.data('current_time', 0);
				// 播放下一首
				playNext();
			}else{
				setButtonStatus({
					obj: obj,
					is_plaing: is_plaing,
					play_btn: play_btn,
				}, true);
				the_working_play_btn = null;
			}
		})
		.on('timeupdate', function(){
			var current_time = player.get(0).currentTime;
			play_btn.data('current_time', current_time);
			time_now.text(formatTime(current_time));
		});
		return player;
	}
	
	// 打印主菜单
	var printMainPage = function(){
		// 分类的HTML
		var category_html = '<li class="play-category">\
			<div class="category-header df">\
				<div class="f1">\
					<span class="category-title">分类列表名称</span>\
					<span class="is_plaing"></span>\
					<span class="new-count">0</span>\
				</div>\
				<div class="w80 tc">\
					<span class="like-count">0</span>\
					<span>/</span>\
					<span class="count">0</span>\
				</div>\
				<div class="w30">\
					<div class="arrow"></div>\
				</div>\
			</div>\
			<ul class="category-body" style="display: none;"></ul>\
		</li>';
		// 音乐的HTML
		var play_box_html = '<li class="play-box">\
			<div class="m-title">音乐文件名称</div>\
			<div class="m-box">\
				<div class="m-box-l">\
					<div class="play-btn">\
						<div class="play-btn-text">播 放</div>\
						<div class="play-btn-time-wrap">\
							<span class="time-now">00:00</span>\
							<span class="time-wave">-</span>\
							<span class="time-to">00:00</span>\
						</div>\
					</div>\
				</div>\
				<div class="m-box-r">\
					<div class="trash-btn wh2d5em mt5"></div>\
					<div class="like-btn"></div>\
				</div>\
			</div>\
			<div class="new-music"></div>\
		</li>';
		
		// <div class="m-audio"><audio class="player"></audio></div>
		
		play_list.html('');
		drag_list.html('');
		audio_wrap.html('');
		
		// 分类计数
		var num = 1;
		// 音乐文件计数
		var music_count = 0;
		// 循环写入主容器
		$.each(json_data, function(key, data) {
			// 设置分类名称到data.key
			var category = $(category_html).data('key', key);
			// 设置分类 计数+标题
			category.find('.category-title').text(key);
			// 分类的[播放]图标
			var is_plaing = category.find('.is_plaing');
			// [喜欢]的数量
			var like_count = 0;
			// [喜欢]的容器
			var line_count_elem = category.find('.like-count');
			// 分类下的音乐组件的容器
			var category_body = category.find('.category-body');
			// 新音乐计数
			var new_count_count = 0;
			// 分类下音乐的计数
			var sub_count = 0;
			// 循环写入音乐到 category_body
			$.each(data, function(i, obj){
				// 跳过被删的音乐
				if (storage_trash.get(obj.path, false)) return true;
				// 生成音乐obj, 保存数据到data, 插入到category_body
				var play_box_elem = $(play_box_html).data('data', obj).appendTo(category_body);
				// [播放]按钮
				var play_btn = play_box_elem.find('.play-btn');
				/*
				// [播放]按钮的文字
				var play_btn_text = play_btn.children('.play-btn-text');
				// [播放]按钮的时间now
				var time_now = play_btn.find('.time-now');
				// [播放]按钮的时间to
				var time_to = play_btn.find('.time-to');
				*/
				// [喜欢]按钮
				var like_btn = play_box_elem.find('.like-btn');
				// [回收]按钮
				var trash_btn = play_box_elem.find('.trash-btn');
				// 设置音乐标题
				var title = obj.name;
				play_box_elem.find('.m-title').text(title);
				
				if (obj['new']===true){
					new_count_count ++;
					play_box_elem.find('.new-music').addClass('show');
				}
				
				// 设置默认属性
				// 暂停
				obj.paused = true;
				
				// 播放按钮被点击时动作
				play_btn.on('click', function(){
					player_box_elem.children().remove();
					//play_list.find('.play-box .play-btn').removeClass('playing').find('.play-btn-text').text('已暂停');
					if (the_working_play_btn && the_working_play_btn != play_btn)the_working_play_btn.trigger('click');
					
					if (obj.paused){
						// 播放
						var player = getPlayObj({
							obj: obj,
							play_btn: play_btn,
							is_plaing: is_plaing,
						});
						player_box_elem.append(player);
						
						// 设置当前工作的播放器按钮为自己
						the_working_play_btn = play_btn;
						
						// 显示播放器
						showPlayer({
							elem: play_box_elem,
							category: key,
							title: title,
							like_btn: like_btn,
							trash_btn: trash_btn,
						});
					}else{
						// 暂停
						the_working_play_btn = false;
						player_box.removeClass('mini');
						setButtonStatus({
							is_plaing: is_plaing,
							play_btn: play_btn,
							obj: obj,
						}, true);
					}
				});
				
				// 存储中得到[喜欢]的值
				var like = storage_like.get(obj.path, false);
				var used_like = storage_used_like.get(obj.path, false);
				if (like && !used_like) {
					obj.like = true;
					// 设置[喜欢]按钮的风格
					like_btn.addClass('like');
					// [喜欢]计数+1
					like_count ++;
				}
				
				if (used_like){
					// 设置[喜欢]按钮的风格
					like_btn.addClass('used-like');
				}else{
					// [喜欢]按钮的动作
					like_btn.on('click', function(){
						var self = $(this);
						var path = obj.path;
						var like = storage_like.get(path, false);
						if (!like){
							// 添加到喜欢
							if (selected_count >= 20){
								$('<div>最多可喜欢20首音乐</div>').dialog({
									title: '提示',
									modal: true,
									buttons: [
										{
											text: '知道啦',
											icon: 'ui-icon-alert',
											click: function() {
												$(this).dialog('destroy');
											}
										},
									]
								});
								return;
							}
							obj.like = true;
							self.addClass('like');
							player_box_btn_like.addClass('like');
							storage_like.set(path, true);
							like_count ++;
						}else{
							// 从喜欢移除
							obj.like = false;
							self.removeClass('like');
							player_box_btn_like.removeClass('like');
							storage_like.set(path, false);
							like_count --;
						}
						line_count_elem.text(like_count);
						// 重置已选择数量
						setSelectedCount();
					});
				}
				
				// 删除按钮
				trash_btn.on('click', function(){
					$('<div>确定删除此音乐?</div>').dialog({
						title: '删除提示',
						modal: true,
						buttons:[
							{
								text: '取消',
								click: function(){
									$(this).dialog('destroy');
									main_menu_wrap.removeClass('show');
								}
							},
							{
								text: '删除',
								class: 'btn-red',
								click: function(){
									$(this).dialog('destroy');
									storage_trash.set(obj.path, true);
									play_box_elem.remove();
									setCategoryInfo(category);
									setSelectedCount();
									hidePlayer();
								}
							},
						],
					});
				});
				sub_count ++;
				music_count ++;
			});
			
			if (new_count_count){
				category.find('.new-count').addClass('show').text(new_count_count);
			}
			
			if (sub_count){
				// 设置分类的拥有歌曲数量
				setCategoryInfo(category);
				// 设置喜欢的计数到分类栏
				line_count_elem.text(like_count);
				num++;
				// 分类添加到主容器
				play_list.append(category);
				// 获取音乐容器的高度
				var outer_height = category_body.outerHeight(true);
				// 保存音乐容器高度到data并显示
				category_body.data('height', outer_height).show();
				// 获取[分类]展开与否的值(key)，默认:0 不展开
				var category_show = storage_category.get(key, 0);
				if (category_show){
					category.addClass('show');
					category_body.height(outer_height);
				}else{
					category_body.height(0);
				}
			}else{
				category.remove();
			}
		});
		
		// 设置标题上的音乐文件数量
		$('.music-count').text(music_count);
		
		setSelectedCount();
	}
	
	// 显示播放器
	var showPlayer = function(json){
		var data = json.elem.data('data');
		if (data.like) {
			player_box_btn_like.addClass('like');
		}else{
			player_box_btn_like.removeClass('like');
		}
		player_box
		.removeClass('mini')
		.addClass('show')
		.data('data', json)
		.find('.music-title')
		.text(json.title);
		
		player_box.find('.music-category').text(json.category);
		player_box_display = true;
		
		$('html,body').css('overflow', 'hidden');
	}
	
	// 隐藏播放器
	var hidePlayer = function(){
		player_box.removeClass('show');
		player_box_display = false;
		$('html,body').css('overflow', '');
	}
	
	//播放下一首
	var playNext = function(){
		var play_boxs = play_list.find('.play-box');
		var next_play_box = null;
		$.each(play_boxs, function(i, play_box){
			play_box = $(play_box);
			if (play_box.hasClass('playing')){
				next_play_box = play_boxs[i+1] || null;
				return false;
			}
		});
		if (next_play_box){
			$(next_play_box).find('.play-btn').trigger('click');
		}else{
			$('<div>以是最后一首歌了</div>').dialog({
				title: '提示',
				modal: true,
				buttons: [
					{
						text: '确定',
						icon: 'ui-icon-alert',
						click: function() {
							$(this).dialog('destroy');
						}
					},
				]
			});
		}
	}
	
	// 播放器事件
	var playerBoxEvent = function(){
		player_box
		.on('click', '.btn-close', function(){
			// 隐藏或最小化播放器
			if (the_working_play_btn){
				// 最小化播放器
				player_box.addClass('mini');
			}else{
				// 隐藏播放器
				player_box.removeClass('mini');
			}
			hidePlayer();
		})
		.on('click', '.btn-like', function(){
			// 喜欢
			var data = player_box.data('data');
			data.like_btn.trigger('click');
		})
		.on('click', '.btn-trash', function(){
			// 删除
			var data = player_box.data('data');
			data.trash_btn.trigger('click');
		})
		.on('click', '.btn-prev', function(){
			// 上一首
			var play_boxs = play_list.find('.play-box');
			var prev_play_box = null;
			$.each(play_boxs, function(i, play_box){
				play_box = $(play_box);
				if (play_box.hasClass('playing')){
					prev_play_box = play_boxs[i-1] || null;
					return false;
				}
			});
			if (prev_play_box){
				$(prev_play_box).find('.play-btn').trigger('click');
			}else{
				$('<div>前面没歌了</div>').dialog({
					title: '提示',
					modal: true,
					buttons: [
						{
							text: '确定',
							icon: 'ui-icon-alert',
							click: function() {
								$(this).dialog('destroy');
							}
						},
					]
				});
			}
		})
		.on('click', '.btn-next', function(){
			playNext();
		});
	}
	
	// 恢复播放器-最大化
	$('.player-mini').on('click', function(){
		//恢复播放器页面
		player_box.removeClass('mini').addClass('show');
		$('html,body').css('overflow', 'hidden');
	});
	
	playerBoxEvent();
	
	// 设置各项被点击动作
	play_list.on('click', '.category-header', function(){
		// 展开收起音乐列表
		var pc = $(this).closest('.play-category');
		var cb = pc.find('.category-body');
		
		// pc.toggleClass('show');
		if (pc.hasClass('show')){
			pc.removeClass('show');
			cb.css('height', 0);
			storage_category.set(pc.data('key'), false);
		}else{
			pc.addClass('show');
			cb.css('height', cb.data('height'));
			storage_category.set(pc.data('key'), true);
		}
	});
	
	// 设置下一步的页面
	var setNextPage = function()
	{
		// 停止正在播放的歌曲
		if (the_working_play_btn)the_working_play_btn.trigger('click');
		player_box.removeClass('show');
		play_list.hide();
		drag_list.show().html('');
		$('.f-menu-0').hide();
		$('.f-menu-1').show();
		audio_wrap.html('');
		
		// 音乐的HTML
		var play_box = '<li class="play-box">\
			<div class="m-title">音乐文件名称</div>\
			<div class="m-box">\
				<div class="m-box-l">\
					<div class="play-btn">\
						<div class="play-btn-text">播放</div>\
						<div class="play-btn-time-wrap">\
							<span class="time-now">00:00</span>\
							<span class="time-wave">-</span>\
							<span class="time-to">00:00</span>\
						</div>\
					</div>\
				</div>\
				<div class="m-box-r tr">\
					<span class="w100 dib mt12">\
						<div class="btn-icon-box up-btn">▲</div>\
						<div class="btn-icon-box ml12 down-btn">▼</div>\
					</span>\
				</div>\
			</div>\
			<div class="m-audio" style="display: none;">\
				<audio class="player"></audio>\
			</div>\
			<div class="front-wrap"></div>\
		</li>';
		
		// 循环写入主容器
		$.each(json_data, function(key, data) {
			// 循环写入音乐到 category_body
			$.each(data, function(i, obj){
				if (!obj.like) return true;
				var play_box_elem = $(play_box).data('data', obj).appendTo(drag_list);
				// [播放]按钮
				var play_btn = play_box_elem.find('.play-btn');
				// [播放]按钮的文字
				var play_btn_text = play_btn.children('.play-btn-text');
				// [播放]按钮的时间now
				var time_now = play_btn.find('.time-now');
				// [播放]按钮的时间to
				var time_to = play_btn.find('.time-to');
				// 设置音乐标题
				play_box_elem.find('.m-title').text('[' + key + '] ' + obj.name);
				// 配置播放obj
				var player = play_box_elem.find('.m-audio audio')
				.text('您的浏览器无法播放')
				.attr('controls', true)
				.attr('autoplay', false)
				.attr('loop', true)
				.on('loadstart', function(){
					play_btn_text.text('准备中');
				})
				.on('loadedmetadata', function(){
					time_to.text(formatTime(player.get(0).duration));
				})
				.on('play', function(){
					// 播放开始时动作
					// 先暂停当前正在播放的音乐
					if (the_working_play_btn) the_working_play_btn.pause()
					// 等待10毫秒继续动作
					setTimeout(function(){
						// 设置播放按钮的文字与风格
						play_box_elem.addClass('playing');
						play_btn_text.text('正在播放');
						// 判断并设置音乐连接地址
						if (!this.src)this.src = obj.url;
						// 设置当前工作的播放器为自己
						the_working_play_btn = player.get(0);
					}, 10);
					
				})
				.on('pause', function(){
					// 播放暂停时动作
					// 设置播放按钮的文字与风格
					play_box_elem.removeClass('playing');
					play_btn_text.text('以暂停');
					// 清空当前工作的播放器
					the_working_play_btn = null;
				})
				.on('timeupdate', function(){
					time_now.text(formatTime(player.get(0).currentTime));
				})
				.appendTo(audio_wrap);
				// 播放按钮被点击时动作
				play_btn.on('click', function(){
					// 设置播放器的地址
					if (!player.attr('src')) player.attr('src', obj.url);
					// 播放器暂停时播放, 播放时暂停
					var player_tag = player.get(0);
					if (player_tag.paused){
						player_tag.play();
					}else{
						player_tag.pause();
					}
				});
			});
		});
		
		drag_list.on('click', '.up-btn', function(){
			var play_box_elem = $(this).closest('.play-box');
			var prev_elem = play_box_elem.prev();
			if (prev_elem.length){
				play_box_elem.insertBefore(prev_elem).find('.front-wrap').css('opacity', '0.5').animate({opacity: 0});
			}
		})
		.on('click', '.down-btn', function(){
			var play_box_elem = $(this).closest('.play-box');
			var next_elem = play_box_elem.next();
			if (next_elem.length){
				play_box_elem.insertAfter(next_elem).find('.front-wrap').css('opacity', '0.5').animate({opacity: 0});
			}
		});
	}
	
	// 返回按钮
	$('.prev-btn').on('click', function(){
		// 停止正在播放的歌曲
		if (the_working_play_btn)the_working_play_btn.pause();
		play_list.show();
		drag_list.hide();
		$('.f-menu-0').show();
		$('.f-menu-1').hide();
		$('.audio-wrap').html('');
	});
	
	// 下一步按钮被点击
	next_btn.on('click', function(){
		if (selected_count < 10){
			$('<div>至少需要喜欢10首音乐</div>').dialog({
				title: '提示',
				modal: true,
				buttons: [
					{
						text: '知道啦',
						icon: 'ui-icon-alert',
						click: function() {
							$(this).dialog('destroy');
						}
					},
				]
			});
			return;
		}
		setNextPage();
		$('html, body').animate({scrollTop:0});
	});
	
	// 完成
	$('.finish-btn').on('click', function(){
		var self_btn = $(this);
		var openInputDialog = function(val){
			var content = $('<div><p>给播放列表起个名字吧</p><p><input type="text" class="create-playlist-input" value="' + (val || '') + '" placeholder="播放列表名称"></input></p></div>');
			var create_playlist_input = content.find('.create-playlist-input');
			content.dialog({
				title: '创建播放列表',
				modal: true,
				buttons: [
					{
						text: '取消',
						click: function() {
							$(this).dialog('destroy');
						}
					},
					{
						text: '确定',
						class: 'btn-blue',
						click: function() {
							var val = create_playlist_input.val();
							var err_msg = '';
							if (val == '') err_msg = '请输入播放列表名称';
							if (/[\/\\\:\*\?"<>\|]/.test(val)) err_msg = '名称中不得包含下列任何字符:<br><span class="err-text">\\/:*?"<>|</span>';
							if (err_msg){
								$('<div>' + err_msg + '</div>').dialog({
									title: '提示',
									modal: true,
									buttons: [
										{
											text: '确定',
											click: function() {
												$(this).dialog('destroy');
												create_playlist_input.focus();
											}
										},
									]
								});
								return;
							}
							doJob(val);
							$(this).dialog('destroy');
						}
					},
				]
			});
		}
		openInputDialog();
		
		var doJob = function(title){
			var arr = [];
			$.each(drag_list.children('.play-box'), function(i, tag){
				var path = $(tag).data('data').path;
				arr.push(path);
			});
			
			// 向服务器发送结果
			$.ajax({
				url: 'save.php',
				type: 'post',
				dataType: 'json',
				data: {
					token: token,
					title: title,
					list: JSON.stringify(arr),
				},
				beforeSend: function(){
					// 显示正在等待
					$.blockUI({
						message: '<div class="waiting"></div>',
						css: {
							width: '50%',
							padding: '10px 30px',
							border: 'none',
							borderRadius: '10px',
							left: '25%',
						}
					});
				},
				success: function(result){
					if (!result.error && result.status !== 1){
						result.error = 999;
						result.message = '未知错误, 非正常请求';
					}
					if (result.error){
						$('<div>').html(result.message).dialog({
							title: '错误:' + result.error,
							modal: true,
							buttons: [
								{
									text: '确定',
									icon: 'ui-icon-alert',
									click: function() {
										if (result.error == 6 || result.error == 7) openInputDialog(title);
										$(this).dialog('destroy');
									}
								},
							]
						});
						return;
					}
					
					// 设置为以喜欢
					$.each(drag_list.children('.play-box'), function(i, tag){
						var path = $(tag).data('data').path;
						storage_used_like.set(path, true);
					});
					
					$('<div>').html(result.message).dialog({
						title: '操作完成',
						modal: true,
						buttons: [
							{
								text: '确定',
								icon: 'ui-icon ui-icon-check',
								click: function() {
									$(this).dialog('destroy');
									$('.prev-btn').trigger('click');
									printMainPage();
								}
							},
						]
					});
				},
				error: function(result){
					//alert('未知错误');
				},
				complete: function(){
					// 隐藏正在等待
					$.unblockUI();
				}
			});

		}
	});
	
	
	var main_menu_wrap = $('.main-menu-wrap');
	// 主菜单弹出隐藏
	$('.main-menu-box').on('click', function(){
		main_menu_wrap.toggleClass('show');
	});
	
	// 重置喜欢
	$('.btn-reset-like').on('click', function(){
		$('<div>是否重置喜欢信息?</div>').dialog({
			title: '重置喜欢',
			modal: true,
			buttons:[
				{
					text: '取消',
					click: function(){
						$(this).dialog('destroy');
						main_menu_wrap.removeClass('show');
					}
				},
				{
					text: '确定',
					click: function(){
						$(this).dialog('destroy');
						main_menu_wrap.removeClass('show');
						storage_like.clear();
						storage_used_like.clear();
						printMainPage();
					}
				},
			],
		});
	});
	// 恢复删除
	$('.btn-trash-like').on('click', function(){
		$('<div>是否恢复以删除的音乐?</div>').dialog({
			title: '恢复删除',
			modal: true,
			buttons:[
				{
					text: '取消',
					click: function(){
						$(this).dialog('destroy');
						main_menu_wrap.removeClass('show');
					}
				},
				{
					text: '恢复',
					class: 'btn-blue',
					click: function(){
						$(this).dialog('destroy');
						main_menu_wrap.removeClass('show');
						storage_trash.clear();
						printMainPage();
					}
				},
			],
		});
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
});