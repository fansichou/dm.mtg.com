var dd = function(data){
	console.log(data);
}
/** storage控制器
 * @param string id 将以此id进行数据的存储
 * return {object Object}
*/
var storageController = function(id, debug)
{
	const base_id = 'local.storage.controller';
	const func_name = 'storageController()';
	
	var self = this;
	
	// 为防止一个页面调用多个storageController时保存信息会互相影响. 因此将使用一个公用参数
	if (typeof(STORAGE_CONTROLLER_BASE_OBJECT) == 'undefined') STORAGE_CONTROLLER_BASE_OBJECT = {};
	
	self.ever_obj = {};
	self.temp_obj = {};
	self.id = id || '___';
	self.debug = debug || false;
	
	// Set ever_obj and temp_obj;
	self.setObj = function()
	{
		// Set ever_obj
		self.ever_obj = STORAGE_CONTROLLER_BASE_OBJECT.ever[self.id] || {};
		// Set temp_obj
		self.temp_obj = STORAGE_CONTROLLER_BASE_OBJECT[self.id] || {};
	}
	
	self.getObj = function(){
		return $.extend(self.temp_obj, self.ever_obj);
	}
	
	self.err = function(val)
	{
		if (!self.debug) return;
		val = func_name + '\n' + val + '\nhttps://docs.google.com/spreadsheets/d/1wTXYFr7ng96bKuTrM9d30dpf3nTCtQx1JnIY2qn7zXY/edit?pli=1#gid=1506898602';
		console.error(val);
	}
	
	if (typeof self.id != 'string' || self.id === '')
	{
		self.err('参数不足。');
		return;
	}else{
		// Get base storage by base_id;
		STORAGE_CONTROLLER_BASE_OBJECT = JSON.parse(window.localStorage.getItem(base_id)) || {ever:{}};
		if (!STORAGE_CONTROLLER_BASE_OBJECT.ever) STORAGE_CONTROLLER_BASE_OBJECT.ever = {};
		self.setObj();
	}
	
	
	// Get a info from base_obj
	self.get = function(key, def)
	{
		if (typeof key != 'string' || key === '') {
			return self.temp_obj;
		}
		var val = '';
		if (def != undefined) val = def;
		var the_obj = self.getObj();
		if (the_obj[key] != undefined || the_obj[key]===0) val = the_obj[key];
		return val;
	}
	
	// Set a info to base_obj
	self.set = function(key, val, ever)
	{
		if (typeof key != 'string' || key === '') {
			self.err('set(); The param is string。');
			return;
		}
		self.setObj();
		if (ever){
			self.ever_obj[key] = val;
			delete(self.temp_obj[key]);
		}else{
			self.temp_obj[key] = val;
			delete(self.ever_obj[key]);
		}
		
		self.save();
	}
	
	// Reset param id
	self.setID = function(key)
	{
		if (typeof key != 'string' || key === '') {
			self.err('setID(); The param is string。');
			return;
		}
		self.id = key;
		self.setObj();
		
	}
	
	// Remove a param in base_obj
	self.remove = function(key)
	{
		if (typeof key != 'string' || key === '') {
			self.err('remove(); The param is string。');
			return;
		}
		
		self.setObj();
		delete(self.ever_obj[key]);
		delete(self.temp_obj[key]);
		self.save();
		
		return self.getObj();
	}
	
	// Save storage
	self.save = function()
	{
		STORAGE_CONTROLLER_BASE_OBJECT.ever[self.id] = self.ever_obj;
		STORAGE_CONTROLLER_BASE_OBJECT[self.id] = self.temp_obj;
		self.saveToStorage(STORAGE_CONTROLLER_BASE_OBJECT);
	}
	
	// Clear the id storage 
	self.clear = function()
	{
		self.setObj();
		self.ever_obj = {};
		self.temp_obj = {};
		self.save();
	}
	
	// Reset storage
	self.reset = function()
	{
		self.saveToStorage({
			ever : STORAGE_CONTROLLER_BASE_OBJECT.ever,
		});
	}
	
	// 只保留此函数所生成的信息, 其他全部删除
	self.init = function(){
		window.localStorage.clear();
		self.reset();
	}
	
	// Save to storage
	self.saveToStorage = function(obj){
		window.localStorage.setItem(base_id, JSON.stringify(obj));
	}
};

/**
 * 判断是否为节点
 * param object
 * return boolean
*/
var isElement = function(obj){
	return (typeof HTMLElement === 'object') 
	?(obj instanceof HTMLElement)
	:!!(obj && typeof obj === 'object' && (obj.nodeType === 1 || obj.nodeType === 9) && typeof obj.nodeName === 'string');
}

/**
 * 返回 jQuery object
 * param string || elem || array
 * return jquery object
 */
var getJQObject = function(param){
	if (param instanceof jQuery){
		return param;
	}else if(param instanceof HTMLElement){
		return $(param);
	}else{
		if ($.isArray(param)) param = param.join();
		if (typeof param == 'string' && $(param).length){
			return $(param);
		}else{
			return false;
		}
	}
} 

/**
 * 替换HTML标签
 */
function escapeHtml(str){
  str = str.replace(/&/g, '&amp;');
  str = str.replace(/>/g, '&gt;');
  str = str.replace(/</g, '&lt;');
  str = str.replace(/"/g, '&quot;');
  str = str.replace(/'/g, '&#x27;');
  str = str.replace(/`/g, '&#x60;');
  return str;
}

/**
 * 去除空数组
 * 非数组参数时,原样返回
 * @param array data
 * @return array
 */
function arrayRemoveNull(data){
	if (!$.isArray(data)) return data;
	return $.grep(data, function(n) {return $.trim(n).length > 0;});
}