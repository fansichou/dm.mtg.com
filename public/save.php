<?php
header("Content-type: application/json; charset=utf-8");

include(dirname(__FILE__) . '\setup.php');
include(APP_PATH . '\const.php');
include(APP_PATH . '\config.php');
include(APP_PATH . '\function.php');

// 不想写类了. 麻烦. 直接写内容
$token = $_SESSION['token'];
// 混合post与get数据
$i = $_POST + $_GET;

$token = $i['token'];

if ($token !== $_SESSION['token']){
	echo json_encode(['error'=>1, 'message'=>'缺少凭证,无法继续.']);
	return;
}

$title = (string)$i['title'];
$list = json_decode($i['list']);

if (!is_array($list) || count($list) < 10 || count($list) > 20){
	echo json_encode(['error'=>2, 'message'=>'文件类型或,数量不符']);
	return;
}

if (!is_dir(MUSIC_PATH)) {
	echo json_encode(['error'=>3, 'message'=>'音乐文件夹不存在']);
	return;
}

// 判断文件夹大小.
$save_forder_size = getFolderSize(SAVE_PATH);
// 限制在20MB之内
if ($save_forder_size > 1024 * 1024 * 20){
	echo json_encode(['error'=>4, 'message'=>'容量超限, 无法继续, 解决此问题, 请联系管理员']);
	return;
}

// 判断播放列表名称
if ($title == '') {
	echo json_encode(['error'=>5, 'message'=>'请输入播放列表名称']);
	return;
}

// 判断有没有特殊符号
if (preg_match('/[\/\\\:\*\?"<>\|]/i', $title)){
	echo json_encode(['error'=>6, 'message'=>'名称中不得包含下列任何字符:<br><span class="err-text">\\/:*?"<>|</span>']);
	return;
}

// 文件名
$file_name = $title;
// 文件路径
$file_path = SAVE_PATH . '/' . iconv('utf-8', 'GBK', $file_name) . '.json';
// 判断文件是否存在
if (file_exists($file_path)){
	echo json_encode(['error'=>7, 'message'=>'列表名称以存在, 请输入其他名称']);
	return;
}

$file = fopen($file_path, 'w');
fwrite($file, json_encode($list));
fclose($file);

echo json_encode([
	'status' => 1, 
	'code' => $file_name,
	'message' => '操作完成, 播放列表名称: <br>' . $file_name
]);