<?php
header("Content-type: application/json; charset=utf-8");

include(dirname(__FILE__) . '\setup.php');
include(APP_PATH . '\const.php');
include(APP_PATH . '\config.php');
include(APP_PATH . '\function.php');

$token = $_SESSION['token'];
// 混合post与get数据
$i = $_POST + $_GET;

$token = $i['token'];

if ($token !== $_SESSION['token']){
	echo json_encode(['error'=>1, 'message'=>'缺少凭证,无法继续.']);
	return;
}

$order = $i['order'];
$order_gbk = iconv('UTF-8', 'GBK', $order);


if ($order == ''){
	echo json_encode(['error'=>2, 'message'=>'参数不足']);
	return;
}

$file_path = SAVE_PATH . '/' . $order_gbk . '.json';

if (!file_exists($file_path)){
	echo json_encode(['error'=>3, 'message'=>'操作单号错误']);
	return;
}

// 新建文件夹
$forder_name = $order;
$dir = SAVE_PATH . '/' . $order_gbk;

if (file_exists($dir)){
	deldir($dir);
}else{
	mkdir($dir, 0777, true);
}

// 读取二进制文件时，需要将第二个参数设置成'rb'
$handle = fopen($file_path, "r");
//通过filesize获得文件大小，将整个文件一下子读到一个字符串中
$contents = fread($handle, filesize($file_path));
fclose($handle);

$list = json_decode($contents, JSON_UNESCAPED_UNICODE);

foreach($list as $i => $data){
	$file_path = iconv('UTF-8', 'GBK', MUSIC_PATH . '/' . $data);
	$new_file_path = iconv('UTF-8', 'GBK', SAVE_PATH . '/' . $order . '/' . ($i < 10? '0': '') . $i . '_' . str_replace('/', '-', $data));
	if (file_exists($file_path)){
		$status = copy($file_path, $new_file_path);
	}
}

echo json_encode([
	'status' => 1, 
	'message' => '操作完成',
]);
?>