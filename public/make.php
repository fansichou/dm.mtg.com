<?php
header("Content-type: text/html; charset=utf-8");

include(dirname(__FILE__) . '\setup.php');
include(APP_PATH . '\const.php');
include(APP_PATH . '\config.php');
include(APP_PATH . '\function.php');
include(APP_PATH . '\DaddyMusic.php');


$_SESSION['token'] = hash("sha256", time());

$dm = new app\DaddyMusic;
$dm::setCreatedList();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>生成列表</title>
<meta http-equiv="x-dns-prefetch-control" content="on">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta content="telephone=no" name="format-detection">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
<?php addJs('jquery-3.4.1.min.js', true);?>
<?php addJs('jquery-ui.min.js', true);?>
<?php addJs('jquery.blockUI.js', true);?>
<?php addJs('common.js');?>
<?php addJs('make.js');?>
<?php addCss('setup.css');?>
<?php addCss('base.css');?>
<?php addCss('jquery-ui.min.css');?>
<script type="text/javascript">
	var token = '<?php echo $_SESSION['token']?>';
	var data = '<?php echo json_encode($dm::$created_list)?>';
</script>
</head>
<body>
	<header>
		<div class="header">爸爸音乐-生成数据</div>
	</header>
	<content>
		<div class="content">
			<div style="position: absolute;width: 100%;left: 0;top: 30%;">
				<div class="p20">
					<select type="text" name="order_name" class="order_input">
						<option value="">选择</option>
					</select>
				</div>
				<div class="p20 tc">
					<div class="btn-box make-btn btn-blue">生成</div>
				</div>
				<div class="p20 tc">
					<div class="btn-box prev-btn btn-gray">返回</div>
				</div>
			</div>
		</div>
	</content>
	<footer>
		
	</footer>
</body>
</html>