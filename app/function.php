<?php
function dd($data = ''){
	print_r($data);
	echo '<br/>';
}

/**
 * PHP判断当前协议是否为HTTPS
 */
function is_https()
{
	if ( !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
		return true;
	} elseif ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
		return true;
	} elseif ( !empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
		return true;
	}
	return false;
}

/**
 * 增加一个JS
 */
function addJs($path = '', $no_timer = false)
{
	$f_path = PUBLIC_PATH . '/js/' . $path;
	if (!file_exists($f_path)) return;
	$path .= $no_timer? '': '?' . filemtime($f_path);
	echo '<script type="text/javascript" charset="utf-8" src="./js/' . $path . '"></script>';
}
/**
 * 增加一个CSS
 */
function addCss($path = '', $no_timer = false)
{
	$f_path = PUBLIC_PATH . '/css/' . $path;
	if (!file_exists($f_path)) return;
	$path .= $no_timer? '': '?' . filemtime($f_path);
	echo '<link type="text/css" href="./css/' . $path . '" rel="stylesheet">';
}

/**
 * 获取文件夹大小
 * @param string $dir
 * @return integer
 */
function getFolderSize($dir){
	$count_size = 0;
	$count = 0;
	$dir_array = scandir($dir);
	foreach($dir_array as $key => $filename){
		if(!in_array($filename, ['','.','..'])){
			if(is_dir($dir . '/' . $filename)){
				$new_foldersize = getFolderSize($dir . '/' . $filename);
				$count_size = $count_size+ $new_foldersize;
			}else if(is_file($dir . '/' . $filename)){
				$count_size = $count_size + filesize($dir . '/' . $filename);
				$count ++;
			}
		}
	}
	return $count_size;
}

/**
 * 清空文件夹
 * @param string path
 */
function deldir($path){
	if(is_dir($path)){
		if (strpos($path, -1) !== '/') $path .= '/';
		$p = scandir($path);
		foreach($p as $val){
			if($val !="." && $val !=".."){
				if(is_dir($path.$val)){
					deldir($path.$val.'/');
					@rmdir($path.$val.'/');
				}else{
					unlink($path.$val);
				}
			}
		}
	}
}
?>