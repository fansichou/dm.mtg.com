<?php
namespace app;
include_once '/getid3/getid3.php';
use getID3;

class DaddyMusic{
	// 音乐文件列表
	public static $files = [];
	// 创建的播放列表
	public static $created_list = [];
	// 可播放音乐文件扩展名
	public static $extension = ['mp3','mp4','m4a'];
	// http ? https
	public static $http_header = '';
	// 初始化
	public function __construct()
	{
		self::$http_header = is_https()? 'https://': 'http://';
		// self::setFileList();
	}
	
	/**
	 * 设置文件列表
	 */
	public static function setFileList()
	{
		if (!is_dir(MUSIC_PATH)) {
			return dd('音乐文件夹不存在');
		}
		$temp_files = scandir(MUSIC_PATH);
		/*
		foreach($temp_files as $i=>$data){
			if (in_array($data, ['','.','..'])) continue;
			//$f_path = self::$path . '\\' . iconv('GBK', 'utf-8', $data);
			$f_path = MUSIC_PATH . '\\' . $data;
			if (file_exists($f_path)){
				$f_name = iconv('GBK', 'utf-8', $data);
				if (self::checkExtension($f_name)){
					$temp = self::getName($f_name);
					$category = substr($temp, 0, strpos($temp, '-'));
					$name = substr($temp, strpos($temp, '-')+1);
					if (empty(self::$files[$category])) self::$files[$category] = [];
					self::$files[$category][] = [
						'url' => self::$http_header . $_SERVER['SERVER_NAME'] .'/' . MUSIC_FORDER . '/' . $f_name,
						'name' => $name,
						'extension' => self::getExtension($f_name),
						'path' => $f_name,
					];
				}
			}
			
		}
		*/
		foreach($temp_files as $i => $data){
			// 跳过非文件夹或文件
			if (in_array($data, ['','.','..'])) continue;
			
			// 分类名称
			$category_name = iconv('GBK', 'utf-8', $data);
			
			// 文件夹路径
			$forder_path = MUSIC_PATH . '/' . $data;
			// 非文件夹时跳过
			if(!is_dir($forder_path)) continue;
			
			// 循环子文件夹
			$files = scandir($forder_path);
			
			foreach($files as $f => $file){
				// 跳过非文件夹或文件
				if (in_array($file, ['','.','..'])) continue;
				
				// 文件名称
				$file_name = iconv('GBK', 'utf-8', $file);
				
				// 文件路径
				$file_dir = $forder_path . '/' . $file;
				
				// 非文件时跳过
				if(is_dir($file_dir)) continue;
				
				$file_path = iconv('GBK', 'utf-8', $data . '/' . $file);
				
				$make_time = filemtime($file_dir);
				// 编辑日期少于两天属于新歌
				$before_time = strtotime('-2 day');
				
				if (self::checkExtension($file_name)){
					// 分类代码
					$category_code = substr($category_name, 0, strpos($category_name, '-'));
					$name = self::getName($file_name);
					if (empty(self::$files[$category_name])) self::$files[$category_name] = [];
					
					$get_id3 = new getID3(); //实例化类
					$f_info = @$get_id3->analyze($file_dir); //分析文件，$path为音频文件的地址
					$file_duration = $f_info['playtime_seconds']; //这个获得的便是音频文件的时长
					
					self::$files[$category_name][] = [
						'url' => self::$http_header . $_SERVER['SERVER_NAME'] .'/' . MUSIC_FORDER . '/' . $category_name . '/' . $file_name,
						'name' => $category_code . $name,
						'extension' => self::getExtension($file_name),
						'path' => $file_path,
						'date' => date('Y-m-d', $make_time),
						'new' => $make_time > $before_time,
						'time' => $file_duration,
					];
				}
			}
			
			//$f_name = iconv('GBK', 'utf-8', $f_path);
			
		}
		
	}
	
	/**
	 * 设置创建的播放列表
	 */
	public static function setCreatedList()
	{
		if (!is_dir(SAVE_PATH)) {
			return dd('播放列表文件夹不存在');
		}
		$temp_files = scandir(SAVE_PATH);
		foreach($temp_files as $i => $data){
			if (in_array($data, ['','.','..'])) continue;
			//$f_path = self::$path . '\\' . iconv('GBK', 'utf-8', $data);
			$f_path = SAVE_PATH . '/' . $data;
			// 跳过文件夹
			if (is_dir($f_path)) continue;
			if (file_exists($f_path)){
				if (self::getExtension($data) == 'json'){
					$name = iconv('GBK', 'utf-8', self::getName($data));
					self::$created_list[] = [
						'name' => $name,
					];
				}
			}
		}
	}
	
	/**
	 * 获取文件扩展名
	 * @param string f_name
	 * @return string
	 */
	public static function getExtension($f_name)
	{
		return pathinfo($f_name, PATHINFO_EXTENSION);
	}
	
	/**
	 * 获得文件名
	 * @param string f_name
	 * @return string
	 */
	public static function getName($f_name)
	{
		return substr($f_name, 0, strrpos($f_name, '.'));
	}
	
	/**
	 * 测试扩展名
	 * @param string f_name
	 * @return boolean
	 */
	public static function checkExtension($f_name = '')
	{
		if (!$f_name) return false;
		return in_array(self::getExtension($f_name), self::$extension);
	}
	
}
?>